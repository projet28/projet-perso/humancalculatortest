import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;


public class JeuPaul {
	
	static final Random R = new Random(System.currentTimeMillis());
	
	private static class Jeu extends JPanel implements ActionListener, ListSelectionListener {
		
		class Al implements ActionListener {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					rep = Integer.parseInt(((JTextField) e.getSource()).getText());
				}catch(Exception ex) {
					rep = 0;
				}
				questionReponse();
			}
			
		}
		
		private static final long serialVersionUID = -5888769194599296126L;
		
		int lvl = 1;
		int op = 2;
		int operand = 1;
		
		JLabel titre = new JLabel("<html>Welcome to the ultimate<br/>human calculator test</html>");
		
		JLabel lvlL = new JLabel("choose a level :");
		JLabel lvlC = new JLabel("1");
		JList<String> listLvl = new JList<String>();
		JLabel opL = new JLabel("<html>choose number of<br/> operation :</html>");
		JLabel opC = new JLabel("2");
		JList<String> listOp = new JList<String>();
		JLabel operandL = new JLabel("<html>choose number of<br/> operand :</html>");
		JLabel operandC = new JLabel("2");
		JList<String> listOperand = new JList<String>();
		
		JButton start = new JButton("start");
		JButton stop = new JButton("stop");
		
		JPanel groupe = new JPanel();
		JScrollPane scrollPane;
		JTextField reponse;
		
		int nbPoint;
		int rep = 0;
		int res;
		
		Jeu(){
			setBackground(Color.BLACK);
			setLayout(null);
			
			titre.setBounds(175, 50, 600, 100);
			titre.setFont(new Font(titre.getFont().getName(), Font.BOLD, 40));
			titre.setForeground(Color.RED);
			add(titre);
			
			String[] lvls = {"1", "2", "3", "4", "5"};
			listLvl = new JList<String>(lvls);
		    listLvl.addListSelectionListener(this);
		    listLvl.setVisibleRowCount(1);
		    listLvl.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		    
			add(lvlL);
			add(listLvl);
			add(lvlC);
			lvlC.setBounds(160, 250, 50, 50);
			lvlL.setBounds(110, 200, 200, 50);
			listLvl.setBounds(120, 300, 100, 20);
			listLvl.setFixedCellWidth(20);
			lvlL.setFont(new Font(titre.getFont().getName(), Font.BOLD, 20));
			lvlC.setFont(new Font(titre.getFont().getName(), Font.BOLD, 20));
			lvlL.setForeground(Color.GREEN);
			lvlC.setForeground(Color.GREEN);
			
			String[] opers = {"2", "3", "4", "5"};
			listOp = new JList<String>(opers);
		    listOp.addListSelectionListener(this);
		    listOp.setVisibleRowCount(1);
		    listOp.setLayoutOrientation(JList.HORIZONTAL_WRAP);
			add(opL);
			add(listOp);
			add(opC);
			opC.setBounds(375, 250, 50, 50);
			opL.setBounds(305, 200, 200, 50);
			listOp.setBounds(345, 300, 80, 20);
			listOp.setFixedCellWidth(20);
			opL.setFont(new Font(titre.getFont().getName(), Font.BOLD, 20));
			opC.setFont(new Font(titre.getFont().getName(), Font.BOLD, 20));
			opL.setForeground(Color.GREEN);
			opC.setForeground(Color.GREEN);
			
			listOperand = new JList<String>(opers);
		    listOperand.addListSelectionListener(this);
		    listOperand.setVisibleRowCount(1);
		    listOperand.setLayoutOrientation(JList.HORIZONTAL_WRAP);
			add(operandL);
			add(listOperand);
			add(operandC);
			operandC.setBounds(600, 250, 50, 50);
			operandL.setBounds(530, 200, 200, 50);
			listOperand.setBounds(570, 300, 80, 20);
			listOperand.setFixedCellWidth(20);
			operandL.setFont(new Font(titre.getFont().getName(), Font.BOLD, 20));
			operandC.setFont(new Font(titre.getFont().getName(), Font.BOLD, 20));
			operandL.setForeground(Color.GREEN);
			operandC.setForeground(Color.GREEN);
			
			add(start);
			start.addActionListener(this);
			start.setBounds(350, 450, 100, 50);
			
			scrollPane = new JScrollPane(groupe, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrollPane.getVerticalScrollBar().setUnitIncrement(16);
			scrollPane.setBounds(0, 100, 805, 475);
			
			groupe.setBackground(Color.BLACK);
			groupe.setLayout(new BoxLayout(groupe, BoxLayout.Y_AXIS));
			
			stop.addActionListener(this);
			stop.setBounds(350, 20, 100, 50);
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == start) {
				remove(titre);
				remove(lvlL);
				remove(listLvl);
				remove(opL);
				remove(listOp);
				remove(operandL);
				remove(listOperand);
				remove(opC);
				remove(lvlC);
				remove(operandC);
				remove(start);
				add(stop);
				
				groupe.removeAll();
				add(scrollPane);
				
				repaint();
		        revalidate();
				start();
			}
			if(e.getSource() == stop) {
				remove(scrollPane);
				remove(stop);
				add(titre);
				add(lvlL);
				add(listLvl);
				add(opL);
				add(listOp);
				add(operandL);
				add(listOperand);
				add(opC);
				add(lvlC);
				add(operandC);
				add(start);
				repaint();
		        revalidate();
			}
		}

		@Override
		public void valueChanged(ListSelectionEvent e) {
			if(e.getSource() == listLvl) {
				lvlC.setText(listLvl.getSelectedValue());
				lvl = Integer.parseInt(listLvl.getSelectedValue());
			}
			if(e.getSource() == listOp) {
				opC.setText(listOp.getSelectedValue());
				op = Integer.parseInt(listOp.getSelectedValue());
			}
			if(e.getSource() == listOperand) {
				operandC.setText(listOperand.getSelectedValue());
				operand = Integer.parseInt(listOperand.getSelectedValue()) -1;
			}
		}
		
		int eqAux(JLabel question) {
			int res = 0;
			int a = lvl*lvl + R.nextInt(lvl*49);
			int b = lvl*lvl + R.nextInt(lvl*49);
			int ope = R.nextInt(op);
			switch(ope) {
			case 0: res = a+b;
					question.setText(question.getText()+"(" + a + " + " + b + ")");
					break;
			case 1: res = a-b;
					question.setText(question.getText()+"(" + a + " - " + b + ")");
					break;
			case 2: res = (a/(6-lvl))*(b/(6-lvl));
					question.setText(question.getText()+a/(6-lvl) + " * " + b/(6-lvl));
					break;
			case 3: a = a/(6-lvl);
					b = b/(6-lvl);
					if(a<b) {
						int tmp = a;
						a = b;
						b = tmp;
					}
					if(b == 0) b=1;
					res = a/b;
					question.setText(question.getText()+a + " / " + b);
					break;
			case 4: b = 2 + R.nextInt(lvl);
					res = (int) Math.pow(a, b);
					question.setText(question.getText()+a + " ^ " + b);
					break;
			}
			return res;
		}
		
		int equation(JLabel question) {
			int res = 0;
			
			switch(operand) {
			case 1: res = eqAux(question); break;
			case 2: int a = lvl*lvl + R.nextInt(lvl*49);
					if(R.nextInt(2) == 0) {
						question.setText(question.getText()+a + " + ");
						res = a + eqAux(question);
					}else {
						question.setText(question.getText()+a + " - ");
						int b = -eqAux(question);
						res = a + b;
					}
					break;
			case 3: a = eqAux(question);
					if(R.nextInt(2) == 0) {
						question.setText(question.getText()+ " + ");
						res = a + eqAux(question);
					}else {
						question.setText(question.getText()+ " - ");
						int b = -eqAux(question);
						res = a + b;
					}
					break;
			case 4: a = lvl*lvl + R.nextInt(lvl*49);
					if(R.nextInt(2) == 0) {
						question.setText(question.getText()+a + " + ");
						int b = eqAux(question);
						if(R.nextInt(2) == 0) {
							question.setText(question.getText()+ " + ");
							res = a + b + eqAux(question);
						}else {
							question.setText(question.getText()+ " - ");
							int c = -eqAux(question);
							res = a + b + c;
						}
					}else {
						question.setText(question.getText()+a + " - ");
						int b = -eqAux(question);
						if(R.nextInt(2) == 0) {
							question.setText(question.getText()+ " + ");
							res = a + b + eqAux(question);
						}else {
							question.setText(question.getText()+ " - ");
							int c = -eqAux(question);
							res = a + b + c;
						}
					}
					break;
			}
			question.setText(question.getText()+" = ? ");
			return res;
		}
		
		void questionReponse() {
			if(nbPoint < 1 || nbPoint > 99) {
				String str = ((JLabel) ((JPanel) groupe.getComponent(groupe.getComponentCount()-1)).getComponent(0)).getText().replace("?", "");
				((JLabel) ((JPanel) groupe.getComponent(groupe.getComponentCount()-1)).getComponent(0)).setText(str);
				((JPanel) groupe.getComponent(groupe.getComponentCount()-1)).remove(reponse);
				((JPanel) groupe.getComponent(groupe.getComponentCount()-1)).setBorder(null);
				JLabel oldRep = new JLabel(""+res);
				oldRep.setFont(new Font(titre.getFont().getName(), Font.BOLD, 20));
				oldRep.setForeground(Color.CYAN);
				((JPanel) groupe.getComponent(groupe.getComponentCount()-1)).add(oldRep);
				
				JPanel resP = new JPanel();
				JLabel resultat = new JLabel();
				if(nbPoint == 0) {
					resultat.setForeground(Color.RED);
					resultat.setText("LOSER");
				}
				if(nbPoint == 100) {
					resultat.setForeground(Color.GREEN);
					resultat.setText("WINNER");
				}
					
				resultat.setFont(new Font(titre.getFont().getName(), Font.BOLD, 20));
				resP.add(resultat);
				resP.setMinimumSize(new Dimension(400, 50));
				resP.setPreferredSize(new Dimension(400, 50));
				resP.setMaximumSize(new Dimension(400, 50));
				resP.setBackground(Color.BLACK);
				resP.setBorder(BorderFactory.createLineBorder(Color.RED));
				groupe.add(resP);
			}else {
				String str = ((JLabel) ((JPanel) groupe.getComponent(groupe.getComponentCount()-1)).getComponent(0)).getText().replace("?", "");
				((JLabel) ((JPanel) groupe.getComponent(groupe.getComponentCount()-1)).getComponent(0)).setText(str);
				((JPanel) groupe.getComponent(groupe.getComponentCount()-1)).remove(reponse);
				((JPanel) groupe.getComponent(groupe.getComponentCount()-1)).setBorder(null);
				JLabel oldRep = new JLabel(""+res);
				oldRep.setFont(new Font(titre.getFont().getName(), Font.BOLD, 20));
				oldRep.setForeground(Color.CYAN);
				((JPanel) groupe.getComponent(groupe.getComponentCount()-1)).add(oldRep);
				
				JPanel resP = new JPanel();
				JLabel resultat = new JLabel();
				if(rep==res) {
					nbPoint++;
					resultat.setForeground(Color.GREEN);
					resultat.setText("Welldone +1 point: " + nbPoint);
				}else {
					nbPoint--;
					resultat.setForeground(Color.RED);
					resultat.setText("Wrong -1 point: " + nbPoint);
				}
				resultat.setFont(new Font(titre.getFont().getName(), Font.BOLD, 20));
				
				resP.add(resultat);
				resP.setBackground(Color.BLACK);
				resP.setMinimumSize(new Dimension(400, 50));
				resP.setPreferredSize(new Dimension(400, 50));
				resP.setMaximumSize(new Dimension(400, 50));
				groupe.add(resP);
				
				JPanel panelRep = new JPanel();
				
				JLabel question = new JLabel();
				res = equation(question);
				question.setFont(new Font(titre.getFont().getName(), Font.BOLD, 20));
				question.setForeground(Color.CYAN);
				panelRep.add(question);
				
				panelRep.setMinimumSize(new Dimension(400, 50));
				panelRep.setPreferredSize(new Dimension(400, 50));
				panelRep.setMaximumSize(new Dimension(400, 50));
				panelRep.setBackground(Color.BLACK);
				panelRep.setBorder(BorderFactory.createLineBorder(Color.RED));
				
				reponse = new JTextField();
				reponse.setPreferredSize(new Dimension(100, 20));
				
				Al al = new Al();
				reponse.addActionListener(al);
				panelRep.add(reponse);
				
				groupe.add(panelRep);
			}
			
			repaint();
	        revalidate();
	        
	        SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getMaximum());
					reponse.requestFocusInWindow();
				}
	        });
		}
		
		void start() {
			nbPoint = 50/lvl;
			
			JPanel pIntro = new JPanel();
			JLabel intro = new JLabel("Level : " + lvl + ", you have : " + nbPoint + " points. Reach 100 score to win without go to 0.");
			intro.setFont(new Font(titre.getFont().getName(), Font.BOLD, 20));
			intro.setForeground(Color.ORANGE);
			
			pIntro.setMaximumSize(new Dimension(700, 50));
			pIntro.setPreferredSize(new Dimension(700, 50));
			pIntro.setBackground(Color.BLACK);
			
			pIntro.add(intro);
			groupe.add(pIntro);
			
			JPanel panelRep = new JPanel();
			
			JLabel question = new JLabel();
			res = equation(question);
			question.setFont(new Font(titre.getFont().getName(), Font.BOLD, 20));
			question.setForeground(Color.CYAN);
			panelRep.add(question);
			
			panelRep.setMinimumSize(new Dimension(400, 50));
			panelRep.setPreferredSize(new Dimension(400, 50));
			panelRep.setMaximumSize(new Dimension(400, 50));
			panelRep.setBackground(Color.BLACK);
			panelRep.setBorder(BorderFactory.createLineBorder(Color.RED));
			
			reponse = new JTextField();
			reponse.setPreferredSize(new Dimension(100, 20));
			
			Al al = new Al();
			reponse.addActionListener(al);
			panelRep.add(reponse);
			groupe.add(panelRep);
			
			repaint();
			revalidate();
			
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					reponse.requestFocusInWindow();
				}
	        });
		}
	}
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("calculator");
		JPanel panel = new Jeu();
		frame.setContentPane(panel);
		frame.setResizable(false);
		frame.pack();
		frame.setBounds(0, 0, 830, 620);
		frame.setLocation(100, 25);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
